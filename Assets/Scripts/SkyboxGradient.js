﻿#pragma strict
import System.IO;
 
class ColorStop
{
        @Range(-1.0, 1.0)
        var curve : float = 0.0;
        @Range(0.0, 1.0)
        var stop : float;
        var color : Color;
}
 
var SIZE : int = 128;
var colors : ColorStop[];
var setFog : boolean = true;
var updateFog : boolean = false;
var saveSkybox : boolean = false;

private var CENTER : float;
 
private var top : Texture2D;
private var bottom : Texture2D;
private var side : Texture2D;
private var currentFogColor : Color;
private var horizon : Color;
 
function Start()
{
        CENTER = (SIZE - 1) / 2.0;
        
        for (var i=0; i < colors.Length; i++)
        	colors[i].color.a = 1;

        top = new Texture2D(SIZE, SIZE);
        bottom = new Texture2D(SIZE, SIZE);
        side = new Texture2D(SIZE, SIZE);
        side.wrapMode = TextureWrapMode.Clamp;
        top.wrapMode = TextureWrapMode.Clamp;
        bottom.wrapMode = TextureWrapMode.Clamp;
        
        if (RenderSettings.skybox==null) RenderSettings.skybox = new Material(Shader.Find ("RenderFX/Skybox"));
       
        var skyMaterial = RenderSettings.skybox;
       
        skyMaterial.SetTexture("_UpTex", top);
        skyMaterial.SetTexture("_DownTex", bottom);
        skyMaterial.SetTexture("_FrontTex", side);
        skyMaterial.SetTexture("_BackTex", side);
        skyMaterial.SetTexture("_LeftTex", side);
        skyMaterial.SetTexture("_RightTex", side);
       
        UpdateSkybox();
        if (saveSkybox)
        {
	        makePNG("side",side);
	        makePNG("bottom", bottom);
	        makePNG("top", top);
        }
        	
        currentFogColor = horizon;
}
 
function Update()
{
		/*if (currentFogColor != horizon) 
		{
			if (RenderSettings.fogColor == currentFogColor)
				RenderSettings.fogColor = horizon;
			currentFogColor = horizon;
			
		}*/
        if (setFog && updateFog) RenderSettings.fogColor = horizon;
}

private function WeightedCurve(val : float, curve : float) : float
{
        var adj : float = Mathf.Exp(-curve);
        return Mathf.Pow(1.0 - Mathf.Pow(1.0 - val, adj), 1.0 / adj);
}

private function MultiColorLerp(val : float) : Color
{
		if(val < colors[0].stop) return colors[0].color;
        for(var i = 1; i < colors.length; i++) {
                if(colors[i].stop >= val) {
                        val = (val - colors[i-1].stop) / (colors[i].stop - colors[i-1].stop);
                        return Color.Lerp(colors[i-1].color, colors[i].color, WeightedCurve(val, colors[i].curve));
                }
        }
        return colors[colors.length - 1].color;
}
 
function UpdateSkybox()
{
        var x : float;
        var y : float = CENTER + 0.25;
        var z : float;
        var x_squared : float;
        var y_squared : float = y * y;
        var mag : float;
        var phi : float;
        var i : int;
        var j : int;
        var flip_i : int;
        var c : Color;
 
        horizon = MultiColorLerp(0.5);
 
        for(i = 0; i < CENTER; i++) {
                x = i - CENTER;
                x_squared = x * x;
                flip_i = SIZE - i - 1;
                for(j = 0; j < SIZE; j++) {
                        z = j - CENTER;
                        mag = Mathf.Sqrt(x_squared + y_squared + (z * z));
                        phi = Mathf.Acos(z / mag);
                        c = MultiColorLerp(phi / Mathf.PI);
                        side.SetPixel(i, j, c);
                        side.SetPixel(flip_i, j, c);
 
                        phi = Mathf.Acos(y / mag); // Coordinate substitution: swap y and z
                        c = MultiColorLerp(phi / Mathf.PI);
                        top.SetPixel(i, j, c);
                        top.SetPixel(flip_i, j, c);
 
                        c = MultiColorLerp(1.0 - (phi / Mathf.PI));
                        bottom.SetPixel(i, j, c);
                        bottom.SetPixel(flip_i, j, c);
                }
        }
        side.Apply();
        top.Apply();
        bottom.Apply();
        
        if (setFog) 
        	RenderSettings.fogColor = horizon;
}
 
function makePNG(name:String,tex:Texture2D)
{
        var bytes = tex.EncodeToPNG();
       
        var file = new System.IO.FileStream(Application.dataPath + "/../" + name + ".png", FileMode.OpenOrCreate);
 
        file.Write(bytes, 0, bytes.Length);
 
        file.Close();
}