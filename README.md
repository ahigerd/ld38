![Diminishing Lands](https://static.jam.vg/raw/320/3/z/2318.png)

Ludum Dare 38
-------------
**Theme:** A Small World

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Diminishing Lands
=================
You are the final caretaker of the Diminishing Lands, a series of small bubble worlds that are slowly decaying away.
Descend into them and rescue the treasures inside before they crumble away forever!

Controls
--------
* Move: Arrow Keys
* Jump: Z
* Pause: Escape

Credits
-------
Programming: [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Sound effects and design: [Adam Higerd](https://ldjam.com/users/coda-highland)

Character sprite base: [Classic Hero](https://opengameart.org/content/classic-hero) by GrafxKid

Coin sprite: [Simple rotating coin](https://opengameart.org/content/simple-rotating-coin) by sacio

Terrain tile set: [Generic Platformer Tileset (16x16)](https://opengameart.org/content/generic-platformer-tileset-16x16-background) by etqws3

Background music: [Underclocked](http://ericskiff.com/music/) by Eric Skiff, licensed under CC-BY

License
=======
Copyright (c) 2017 Adam Higerd and Austin Allman

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
